package ch.bwe.fad.v1.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation for mail message.
 *
 * @author Benjamin Weber
 */
class MailMessageImpl implements MailMessage {

  private String sender;
  private List<String> recipients = new LinkedList<>();
  private List<String> copyRecipients = new LinkedList<>();
  private List<String> blindCopyRecipients = new LinkedList<>();
  private String subject;
  private String message;
  private LocalDateTime sendDateTime;
  private LocalDateTime receivedDateTime;

  MailMessageImpl() {
  }

  @Override
  public void setSender(String sender) {
    this.sender = sender;
  }

  @Override
  public String getSender() {
    return sender;
  }

  @Override
  public void setRecipients(Collection<String> recipients) {
    this.recipients.clear();
    this.recipients.addAll(recipients);
  }

  @Override
  public void addRecipient(String recipient) {
    this.recipients.add(recipient);
  }

  @Override
  public List<String> getRecipients() {
    return Collections.unmodifiableList(recipients);
  }

  @Override
  public void setCopyRecipients(Collection<String> copyRecipients) {
    this.copyRecipients.clear();
    this.copyRecipients.addAll(copyRecipients);
  }

  @Override
  public void addCopyRecipient(String copyRecipient) {
    this.copyRecipients.add(copyRecipient);
  }

  @Override
  public List<String> getCopyRecipients() {
    return Collections.unmodifiableList(copyRecipients);
  }

  @Override
  public void setBlindCopyRecipients(Collection<String> blindCopyRecipients) {
    this.blindCopyRecipients.clear();
    this.blindCopyRecipients.addAll(blindCopyRecipients);
  }

  @Override
  public void addBlindCopyRecipient(String blindCopyRecipient) {
    this.blindCopyRecipients.add(blindCopyRecipient);
  }

  @Override
  public List<String> getBlindCopyRecipients() {
    return Collections.unmodifiableList(blindCopyRecipients);
  }

  @Override
  public void setSubject(String subject) {
    this.subject = subject;
  }

  @Override
  public String getSubject() {
    return subject;
  }

  @Override
  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String getMessage() {
    return message;
  }

  @Override
  public void setSendDateTime(LocalDateTime sendDateTime) {
    this.sendDateTime = sendDateTime;
  }

  @Override
  public LocalDateTime getSendDateTime() {
    return sendDateTime;
  }

  @Override
  public void setReceivedDateTime(LocalDateTime receivedDateTime) {
    this.receivedDateTime = receivedDateTime;
  }

  @Override
  public LocalDateTime getReceivedDateTime() {
    return receivedDateTime;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append("From: ");
    builder.append(sender);

    builder.append("\nTo: ");
    builder.append(recipients);

    builder.append("\nCC: ");
    builder.append(copyRecipients);

    builder.append("\nBCC: ");
    builder.append(blindCopyRecipients);

    builder.append("\nSubject: ");
    builder.append(subject);

    builder.append("\nSent: ");
    builder.append(sendDateTime);

    builder.append("\nReceived: ");
    builder.append(receivedDateTime);

    builder.append("\n\n");
    builder.append(message);

    return builder.toString();
  }

}
