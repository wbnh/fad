package ch.bwe.fad.v1.model;

/**
 * Configuration for SMTP.
 *
 * @author Benjamin Weber
 */
public interface SmtpConfiguration {

  /**
   * @return the host name of the SMTP server.
   */
  String mailSmtpHost();

  /**
   * @return the port to connect to.
   */
  Integer mailSmtpPort();

  /**
   * @return whether the connection is encrypted.
   */
  Boolean mailSmtpAuth();

  /**
   * @return the username for authentication.
   */
  String mailSmtpUser();

  /**
   * @return the password for authentication.
   */
  String mailSmtpPwd();
}
