package ch.bwe.fad.v1.model;

/**
 * Configuration for IMAP.
 *
 * @author Benjamin Weber
 */
public interface ImapConfiguration {

  /**
   * @return the host name of the IMAP server.
   */
  String mailImapHost();

  /**
   * @return the port to connect to.
   */
  Integer mailImapPort();

  /**
   * @return the password to use for authentication.
   */
  String mailImapPwd();

  /**
   * @return whether to use an encrypted connection.
   */
  Boolean mailImapSsl();

  /**
   * @return the username for authentication.
   */
  String mailImapUser();
}
