package ch.bwe.fad.v1.model;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Model to represent a mail message.
 *
 * @author Benjamin Weber
 */
public interface MailMessage {

  /**
   * @param sender the address which sends the message
   */
  void setSender(String sender);

  /**
   * @return the address which sends the message.
   */
  String getSender();

  /**
   * @param recipients the TO recipients of the message
   */
  void setRecipients(Collection<String> recipients);

  /**
   * @param recipient the TO recipient to add
   */
  void addRecipient(String recipient);

  /**
   * @return all the TO recipients of the message
   */
  List<String> getRecipients();

  /**
   * @param copyRecipients the CC recipients of the message
   */
  void setCopyRecipients(Collection<String> copyRecipients);

  /**
   * @param copyRecipient the CC recipient to add
   */
  void addCopyRecipient(String copyRecipient);

  /**
   * @return all the CC recipients of the message.
   */
  List<String> getCopyRecipients();

  /**
   * @param blindCopyRecipients the BCC recipients of the message
   */
  void setBlindCopyRecipients(Collection<String> blindCopyRecipients);

  /**
   * @param blindCopyRecipient the BCC recipient to add
   */
  void addBlindCopyRecipient(String blindCopyRecipient);

  /**
   * @return all the BCC recipients of the message.
   */
  List<String> getBlindCopyRecipients();

  /**
   * @param subject the subject of the message
   */
  void setSubject(String subject);

  /**
   * @return the subject of the message.
   */
  String getSubject();

  /**
   * @param message the effective message text
   */
  void setMessage(String message);

  /**
   * @return the effective message text.
   */
  String getMessage();

  /**
   * @param sendDateTime the time the message was sent
   */
  void setSendDateTime(LocalDateTime sendDateTime);

  /**
   * @return the time the message was sent.
   */
  LocalDateTime getSendDateTime();

  /**
   * @param receivedDateTime the time the message was received
   */
  void setReceivedDateTime(LocalDateTime receivedDateTime);

  /**
   * @return the time the message was received.
   */
  LocalDateTime getReceivedDateTime();
}
