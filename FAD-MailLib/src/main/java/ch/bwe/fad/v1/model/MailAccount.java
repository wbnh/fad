package ch.bwe.fad.v1.model;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.util.AssertUtils;

/**
 * Represents a mail account - sends and receives mail.
 *
 * @author Benjamin Weber
 */
public class MailAccount {

  private final class CustomAuthenticator extends Authenticator {
    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication(smtpConfiguration.getProperty(SMTP_USER),
          smtpConfiguration.getProperty(SMTP_PASSWORD));
    }
  }

  private static final String IMAP_PROTOCOL = "imap";
  private static final String SMTP_PROTOCOL = "smtp";
  private static final String CONFIG_IMAP_PROTOCOL = "mail.store.protocol";

  private final static String CONFIG_SMTP_AUTH = "mail.smtp.auth";
  private final static String CONFIG_SMTP_HOST = "mail.smtp.host";
  private final static String CONFIG_SMTP_PORT = "mail.smtp.port";
  private static final String CONFIG_SMTP_TLS = "mail.smtp.starttls.enable";
  private final static String CONFIG_SMTP_PROTOCOL = "mail.transport.protocol";

  private static final String CONFIG_IMAP_TLS = "mail.imap.starttls.enable";
  private static final String CONFIG_IMAP_PORT = "mail.imap.port";
  private static final String IMAP_SOCKETFACTORY_PROVIDER = "ssl.SocketFactory.provider";

  private static final String IMAP_HOST = "mailImapHost";
  private static final String IMAP_PORT = "mailImapPort";
  private static final String IMAP_SSL = "mailImapSsl";
  private static final String IMAP_USER = "mailImapUser";
  private static final String IMAP_PASSWORD = "mailImapPwd";
  private static final String SMTP_HOST = "mailSmtpHost";
  private static final String SMTP_PORT = "mailSmtpPort";
  private static final String SMTP_AUTH = "mailSmtpAuth";
  private static final String SMTP_USER = "mailSmtpUser";
  private static final String SMTP_PASSWORD = "mailSmtpPwd";

  private static final String DEFAULT_FOLDER_NAME = "inbox";

  private Properties imapConfiguration;
  private Properties smtpConfiguration;

  private Session session;
  private Store store;

  /**
   * Connect to the remote servers to perform email operations.
   * 
   * @param imapConfig the configuration for the IMAP server
   * @param smtpConfig the configuration for the SMTP server
   */
  public void connect(ImapConfiguration imapConfig, SmtpConfiguration smtpConfig) {
    Properties imap = new Properties();
    imap.setProperty(IMAP_HOST, imapConfig.mailImapHost());
    imap.setProperty(IMAP_PORT, Integer.toString(imapConfig.mailImapPort()));
    imap.setProperty(IMAP_SSL, Boolean.toString(imapConfig.mailImapSsl()));
    imap.setProperty(IMAP_USER, imapConfig.mailImapUser());
    imap.setProperty(IMAP_PASSWORD, imapConfig.mailImapPwd());

    Properties smtp = new Properties();
    smtp.setProperty(SMTP_HOST, smtpConfig.mailSmtpHost());
    smtp.setProperty(SMTP_PORT, Integer.toString(smtpConfig.mailSmtpPort()));
    smtp.setProperty(SMTP_AUTH, Boolean.toString(smtpConfig.mailSmtpAuth()));
    smtp.setProperty(SMTP_USER, smtpConfig.mailSmtpUser());
    smtp.setProperty(SMTP_PASSWORD, smtpConfig.mailSmtpPwd());

    connect(imap, smtp);
  }

  /**
   * Connect to the remote servers to perform email operations.
   * 
   * @param imapConfig the configuration for the IMAP server
   * @param smtpConfig the configuration for the SMTP server
   */
  public void connect(Properties imapConfig, Properties smtpConfig) {
    AssertUtils.notNull(imapConfig);
    AssertUtils.notNull(smtpConfig);

    imapConfiguration = imapConfig;
    smtpConfiguration = smtpConfig;

    Properties connectionProperties = new Properties();

    connectionProperties.setProperty(CONFIG_IMAP_PROTOCOL, IMAP_PROTOCOL);

    if (Boolean.TRUE.equals(Boolean.valueOf(imapConfiguration.getProperty(IMAP_SSL)))) {
      connectionProperties.setProperty(CONFIG_IMAP_TLS, "true");
      if (imapConfiguration.getProperty(IMAP_PORT) != null) {
        connectionProperties.setProperty(CONFIG_IMAP_PORT, imapConfiguration.getProperty(IMAP_PORT));
      }
      String className = MailSSLSocketFactory.class.getName();
      connectionProperties.setProperty(IMAP_SOCKETFACTORY_PROVIDER, className);
      connectionProperties.setProperty("mail." + IMAP_PROTOCOL + ".socketFactory.class", className);
    }

    session = Session.getInstance(connectionProperties);

    try {
      store = session.getStore(IMAP_PROTOCOL);

      if (imapConfiguration.getProperty(IMAP_PORT) == null) {
        store.connect(imapConfiguration.getProperty(IMAP_HOST), imapConfiguration.getProperty(IMAP_USER),
            imapConfiguration.getProperty(IMAP_PASSWORD));
      } else {
        store.connect(imapConfiguration.getProperty(IMAP_HOST),
            Integer.valueOf(imapConfiguration.getProperty(IMAP_PORT)), imapConfiguration.getProperty(IMAP_USER),
            imapConfiguration.getProperty(IMAP_PASSWORD));
      }
    } catch (MessagingException e) {
      ServiceRegistry.getLogProxy().error(this, "Could connect to emails", e);
      throw new RuntimeException(e);
    }
  }

  /**
   * @return whether we are connected to the remote servers.
   */
  public boolean isConnected() {
    return store != null && store.isConnected();
  }

  /**
   * Terminates the connection to the remote servers.
   */
  public void disconnect() {
    if (store != null) {
      try {
        store.close();
      } catch (MessagingException e) {
        ServiceRegistry.getLogProxy().error(this, "Could not disconnect mail store", e);
        throw new RuntimeException(e);
      }
    }
  }

  private InternetAddress[] stringsToAddresses(List<String> strings) throws AddressException {
    if (strings == null || strings.isEmpty()) { return null; }

    InternetAddress[] addresses = new InternetAddress[strings.size()];

    for (int i = 0; i < strings.size(); i++) {
      addresses[i] = new InternetAddress(strings.get(i));
    }

    return addresses;
  }

  /**
   * Sends a mail message to the configured recipients.
   * 
   * @param mail              the message to send
   * @param maxSendTries      how many times we want to try to send the message in
   *                          case of problems
   * @param sendTryWaitMillis how long we want to wait between tries in
   *                          milliseconds
   */
  public void sendMail(MailMessage mail, int maxSendTries, int sendTryWaitMillis) {

    MimeMessage message = null;

    Properties smtpProperties = new Properties();
    smtpConfigurationToProperties(smtpProperties);

    Authenticator authenticator = null;
    if (Boolean.valueOf(smtpConfiguration.getProperty(SMTP_AUTH))) {
      smtpProperties.setProperty(CONFIG_SMTP_TLS, "true");
      authenticator = new CustomAuthenticator();
    }

    Session smtpSession = Session.getInstance(smtpProperties, authenticator);

    message = new MimeMessage(smtpSession);

    try {

      message.setSubject(mail.getSubject());
      message.setSender(new InternetAddress(mail.getSender()));
      message.setFrom(new InternetAddress(mail.getSender()));

      InternetAddress[] toRecipients = stringsToAddresses(mail.getRecipients());

      if (toRecipients == null) { throw new IllegalArgumentException(
          "A mail message must have at least one recipient"); }

      message.setRecipients(RecipientType.TO, toRecipients);
      InternetAddress[] ccRecipients = stringsToAddresses(mail.getCopyRecipients());
      if (ccRecipients != null) {
        message.setRecipients(RecipientType.CC, ccRecipients);
      }
      InternetAddress[] bccRecipients = stringsToAddresses(mail.getBlindCopyRecipients());
      if (bccRecipients != null) {
        message.setRecipients(RecipientType.BCC, bccRecipients);
      }

      MimeMultipart content = new MimeMultipart();

      MimeBodyPart textContent = new MimeBodyPart();
      textContent.setContent(mail.getMessage(), "text/html");

      content.addBodyPart(textContent);

      message.setContent(content);

    } catch (MessagingException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not assemble message", e);
      throw new RuntimeException(e);
    }

    for (int i = 0; i < maxSendTries; i++) {
      try {

        Transport.send(message);
        break;
      } catch (MessagingException e) {
        if (i >= maxSendTries - 1) {
          ServiceRegistry.getLogProxy().error(this, "Could not send message", e);
          throw new RuntimeException(e);
        }
        ServiceRegistry.getLogProxy().debug(this, "Could not send message, retrying");
      }

      try {
        Thread.sleep(sendTryWaitMillis);
      } catch (InterruptedException e) {
        ServiceRegistry.getLogProxy().debug(this, "Interrupted while waiting, continuing");
      }
    }
  }

  private void smtpConfigurationToProperties(Properties smtpProperties) {
    smtpProperties.setProperty(CONFIG_SMTP_HOST, smtpConfiguration.getProperty(SMTP_HOST));
    smtpProperties.setProperty(CONFIG_SMTP_PORT, smtpConfiguration.getProperty(SMTP_PORT));
    smtpProperties.setProperty(CONFIG_SMTP_PROTOCOL, SMTP_PROTOCOL);
    smtpProperties.setProperty(CONFIG_SMTP_AUTH, smtpConfiguration.getProperty(SMTP_AUTH));
  }

  /**
   * Reads all mail from the default folder.
   * 
   * @return a stream of the messages
   */
  public Stream<MailMessage> readMail() {
    return readMail(DEFAULT_FOLDER_NAME);
  }

  /**
   * Reads all mail from the passed folder.
   * 
   * @param folder the folder to look at
   * @return a stream of the messages
   */
  public Stream<MailMessage> readMail(String folder) {
    try {
      Folder root = store.getFolder(folder);
      AssertUtils.assertTrue(root.exists());
      root.open(Folder.READ_ONLY);
      Message[] messages = root.getMessages();

      return Stream.of(messages).map(MailAccount::messageToMailMessage).onClose(() -> {
        try {
          root.close(false);
        } catch (MessagingException e) {
          ServiceRegistry.getLogProxy().error(this, "Could not close folder", e);
        }
      });

    } catch (MessagingException e) {
      ServiceRegistry.getLogProxy().error(this, "Could not read messages", e);
      return Stream.empty();
    }
  }

  private static MailMessage messageToMailMessage(Message message) {
    MailMessageBuilder builder = new MailMessageBuilder();

    try {

      Address[] froms = message.getFrom();
      if (froms != null && froms.length > 0) {
        builder.sender(froms[0].toString());
      }

      Address[] recipients = message.getRecipients(RecipientType.TO);
      if (recipients != null && recipients.length > 0) {
        List<String> list = new LinkedList<>();
        Arrays.stream(recipients).forEach(r -> list.add(r.toString()));
        builder.recipients(list);
      }

      Address[] copyRecipients = message.getRecipients(RecipientType.CC);
      if (copyRecipients != null && copyRecipients.length > 0) {
        List<String> list = new LinkedList<>();
        Arrays.stream(copyRecipients).forEach(r -> list.add(r.toString()));
        builder.recipients(list);
      }

      Address[] blindCopyRecipients = message.getRecipients(RecipientType.BCC);
      if (blindCopyRecipients != null && blindCopyRecipients.length > 0) {
        List<String> list = new LinkedList<>();
        Arrays.stream(blindCopyRecipients).forEach(r -> list.add(r.toString()));
        builder.recipients(list);
      }

      builder.subject(message.getSubject());

      String contentType = message.getContentType();
      if (message.isMimeType("text/*")) {
        try {
          builder.message((String) message.getContent());
        } catch (IOException e) {
          ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not read text content from message", e);
        }
      } else if (message.isMimeType("multipart/*")) {
        MimeMultipart content;
        try {
          content = (MimeMultipart) message.getContent();
          StringBuilder text = new StringBuilder();
          for (int i = 0; i < content.getCount(); i++) {
            BodyPart part = content.getBodyPart(i);
            if (part.isMimeType("text/*")) {
              try {
                text.append(part.getContent());
              } catch (IOException e) {
                ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not read content from mime multipart", e);
              }
            }
          }
          builder.message(text.toString());
        } catch (IOException contentProblem) {
          ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not get content from message", contentProblem);
        }
      } else {
        ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not handle content type {0}", null, contentType);
      }

    } catch (MessagingException e) {
      ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not read from message", e);
    }

    MailMessage mailMessage = builder.build();

    try {

      Date sentDate = message.getSentDate();
      if (sentDate != null) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(sentDate.toInstant(), ZoneId.systemDefault());
        mailMessage.setSendDateTime(dateTime);
      }

      Date receivedDate = message.getReceivedDate();
      if (receivedDate != null) {
        LocalDateTime dateTime = LocalDateTime.ofInstant(receivedDate.toInstant(), ZoneId.systemDefault());
        mailMessage.setReceivedDateTime(dateTime);
      }

    } catch (MessagingException e) {
      ServiceRegistry.getLogProxy().error(MailAccount.class, "Could not read from message", e);
    }

    return mailMessage;
  }

}
