package ch.bwe.fad.v1.model;

import java.util.Collection;

/**
 * Builder to create mail messages manually.
 *
 * @author Benjamin Weber
 */
public class MailMessageBuilder {

  private final MailMessage message;
  private boolean built = false;

  /**
   * Constructor.
   */
  public MailMessageBuilder() {
    message = new MailMessageImpl();
  }

  /**
   * @param sender the sending address
   * @return this for chaining.
   */
  public MailMessageBuilder sender(String sender) {
    ensureNotBuilt();
    message.setSender(sender);
    return this;
  }

  /**
   * @param recipient a TO recipient
   * @return this for chaining.
   */
  public MailMessageBuilder recipient(String recipient) {
    ensureNotBuilt();
    message.addRecipient(recipient);
    return this;
  }

  /**
   * @param recipients all the TO recipients
   * @return this for chaining.
   */
  public MailMessageBuilder recipients(Collection<String> recipients) {
    ensureNotBuilt();
    message.setRecipients(recipients);
    return this;
  }

  /**
   * @param copyRecipient a CC recipient
   * @return this for chaining.
   */
  public MailMessageBuilder copyRecipient(String copyRecipient) {
    ensureNotBuilt();
    message.addCopyRecipient(copyRecipient);
    return this;
  }

  /**
   * @param copyRecipients all the CC recipients
   * @return this for chaining.
   */
  public MailMessageBuilder copyRecipients(Collection<String> copyRecipients) {
    ensureNotBuilt();
    message.setCopyRecipients(copyRecipients);
    return this;
  }

  /**
   * @param blindCopyRecipient a BCC recipient
   * @return this for chaining.
   */
  public MailMessageBuilder blindCopyRecipient(String blindCopyRecipient) {
    ensureNotBuilt();
    message.addBlindCopyRecipient(blindCopyRecipient);
    return this;
  }

  /**
   * @param blindCopyRecipients all the BCC recipients
   * @return this for chaining.
   */
  public MailMessageBuilder blindCopyRecipients(Collection<String> blindCopyRecipients) {
    ensureNotBuilt();
    message.setBlindCopyRecipients(blindCopyRecipients);
    return this;
  }

  /**
   * @param subject the message subject
   * @return this for chaining.
   */
  public MailMessageBuilder subject(String subject) {
    ensureNotBuilt();
    message.setSubject(subject);
    return this;
  }

  /**
   * @param message the text to send
   * @return this for chaining.
   */
  public MailMessageBuilder message(String message) {
    ensureNotBuilt();
    this.message.setMessage(message);
    return this;
  }

  /**
   * @return the created message.
   */
  public MailMessage build() {
    ensureNotBuilt();
    built = true;
    return message;
  }

  private void ensureNotBuilt() {
    if (built) { throw new IllegalStateException("This builder has already built an object"); }
  }
}
