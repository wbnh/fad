package ch.bwe.fad.v1.model;

import java.util.stream.Stream;

import org.junit.Test;

import ch.bwe.faa.v1.core.service.ServiceRegistry;
import ch.bwe.faa.v1.core.service.configuration.ConfigurationProperties;

/**
 * Test class to test mail account.
 *
 * @author Benjamin Weber
 */
public class TestClassMailAccount {

  private MailAccount createAccount() {
    MailAccount account = new MailAccount();

    account.connect(
        ServiceRegistry.getTypedConfigurationProxy().get(new ConfigurationProperties<>(ImapConfiguration.class)),
        ServiceRegistry.getTypedConfigurationProxy().get(new ConfigurationProperties<>(SmtpConfiguration.class)));

    return account;
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testSendMail() throws Exception {
    MailAccount account = createAccount();

    try {
      MailMessageBuilder builder = new MailMessageBuilder();

      builder.subject("FAD Test");
      builder.sender("benjamin.n.weber@gmail.com");
      builder.recipient("benjamin.n.weber@gmail.com");
      builder.message("This is a test email from the FAD framework.");

      account.sendMail(builder.build(), 1, 0);

    } finally {
      account.disconnect();
    }
  }

  /**
   * @throws Exception if a problem occurs
   */
  @Test
  public void testReadMail() throws Exception {
    MailAccount account = createAccount();

    try {
      Stream<MailMessage> stream = account.readMail();

      stream.forEach(System.out::println);
    } finally {
      account.disconnect();
    }
  }
}
